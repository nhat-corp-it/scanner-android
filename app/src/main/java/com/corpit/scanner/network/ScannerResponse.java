package com.corpit.scanner.network;

import com.squareup.moshi.Json;

public class ScannerResponse<T> {

    @Json(name = "data")
    private T data;

    @Json(name = "message")
    private String message;

    @Json(name = "Status")
    private String status;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
