package com.corpit.scanner.network;

import com.corpit.scanner.BuildConfig;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

public class ScannerClient {

    private static final String ROOT_URL = BuildConfig.API_ROOT_URL;

    private static ScannerService apiService = null;

    private ScannerClient() {
    }

    private static Retrofit initRetrofit() {
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(10, TimeUnit.SECONDS)
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .build();

        return new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .addConverterFactory(MoshiConverterFactory.create())
                .client(okHttpClient)
                .build();
    }

    /**
     * Get API Service
     *
     * @return API Service
     */
    public static ScannerService getApiService() {
        if (apiService == null) {
            synchronized (ScannerClient.class) {
                if (apiService == null) {
                    apiService = initRetrofit().create(ScannerService.class);
                }
            }
        }

        return apiService;
    }
}
