package com.corpit.scanner.network;

import android.support.annotation.NonNull;

import com.corpit.scanner.model.Transaction;
import com.corpit.scanner.util.DateConverter;

import java.util.HashMap;
import java.util.Map;

public class QueryMaps {

    private QueryMaps() {
    }

    @NonNull
    public static Map<String, String> uploadRecord(Transaction transaction) {
        Map<String, String> map = new HashMap<>();
        map.put("SID", transaction.getSessionId());
        map.put("UserId", transaction.getVisitorId());
        map.put("ScanTime", DateConverter.toText(transaction.getTimestamp(), DateConverter.SERVER_DATE_FORMAT));
        map.put("ScanDevice", transaction.getDeviceName());
        map.put("ScanType", transaction.getType());
        map.put("RegType", transaction.getRegistrationType());

        return map;
    }
}
