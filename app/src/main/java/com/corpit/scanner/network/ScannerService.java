package com.corpit.scanner.network;

import com.corpit.scanner.BuildConfig;
import com.corpit.scanner.model.Session;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface ScannerService {

    @GET("masterapi.aspx?type=sessionlist&showid=" + BuildConfig.SHOW_ID)
    Call<ScannerResponse<List<Session>>> getConferenceList();

    @GET("masterapi.aspx?type=checkpermission")
    Call<ScannerResponse<Boolean>> checkPermission(@Query("UserID") String visitorId,
                                                   @Query("SID") String sessionId);

    @GET("masterapi.aspx?type=savescandata")
    Call<ScannerResponse<Boolean>> uploadRecord(@QueryMap Map<String, String> map);

}
