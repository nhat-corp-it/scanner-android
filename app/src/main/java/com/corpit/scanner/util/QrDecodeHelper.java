package com.corpit.scanner.util;

import android.support.annotation.IntDef;

import com.corpit.scanner.BuildConfig;
import com.corpit.scanner.model.Visitor;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class QrDecodeHelper {

    private static final int DECODE_KEY = BuildConfig.DECODE_KEY;

    @IntDef({QrCodeType.EXHIBITOR, QrCodeType.VISITOR})
    @Retention(RetentionPolicy.SOURCE)
    public @interface QrCodeType {
        int EXHIBITOR = 5;
        int VISITOR = 10;
    }

    private static final String SEPARATOR = "^";

    private static final String SEPARATOR_REGEX = "\\^";

    private QrDecodeHelper() {
    }

    public static String encrypt(String string) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < string.length(); i++) {
            char tempChar = string.charAt(i);
            if ((int) tempChar + DECODE_KEY > 126) {
                result.append((char) ((((int) tempChar + DECODE_KEY) - 127) + 32));
            } else {
                result.append((char) ((int) tempChar + DECODE_KEY));
            }
        }

        return result.toString();
    }

    public static String decrypt(String string) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < string.length(); i++) {
            char tempChar = string.charAt(i);
            if ((int) tempChar - DECODE_KEY < 32) {
                result.append((char) ((((int) tempChar - DECODE_KEY) + 127) - 32));
            } else {
                result.append((char) ((int) tempChar - DECODE_KEY));
            }
        }

        return result.toString();
    }

    public static boolean isValidQrCode(String decodedString, @QrCodeType int type) {
        switch (type) {
            case QrCodeType.EXHIBITOR:
                return decodedString.startsWith("e") && decodedString.split(SEPARATOR_REGEX).length == 9;
            case QrCodeType.VISITOR:
                return decodedString.startsWith("v") && decodedString.split(SEPARATOR_REGEX).length == 8;
            default:
                throw new IllegalArgumentException("Scan type not supported.");
        }
    }

    public static Visitor toVisitor(String decodedString) {
        String[] fields = decodedString.substring(1).split(SEPARATOR_REGEX);
        Visitor visitor = new Visitor();
        visitor.setName(fields[0]);
        visitor.setEmail(fields[1]);
        visitor.setCompany(fields[2]);
        visitor.setCountry(fields[3]);
        visitor.setJobTitle(fields[4]);
        visitor.setPhone(fields[5]);
        visitor.setRegisterId(fields[6]);
        visitor.setPermissions(fields[7]);

        visitor.setQrCode(decodedString);

        return visitor;
    }
}
