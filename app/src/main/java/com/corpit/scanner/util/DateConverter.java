package com.corpit.scanner.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public final class DateConverter {

    public static final DateFormat SERVER_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-DD HH:mm:ss.SSS ZZZZ", Locale.UK);
    public static final DateFormat CLIENT_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.UK);

    private DateConverter() {
    }

    public static String toText(long timestamp, DateFormat dateFormat) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);

        return dateFormat.format(calendar.getTime());
    }
}
