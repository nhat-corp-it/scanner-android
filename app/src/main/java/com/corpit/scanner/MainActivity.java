package com.corpit.scanner;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.constraint.Group;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.TooltipCompat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.corpit.scanner.ScanActivity.ScanType;
import com.corpit.scanner.model.Session;
import com.corpit.scanner.model.SessionDao;
import com.corpit.scanner.model.Transaction;
import com.corpit.scanner.model.Transaction.RegistrationType;
import com.corpit.scanner.model.Transaction.TransactionType;
import com.corpit.scanner.model.Visitor;
import com.corpit.scanner.network.ScannerClient;
import com.corpit.scanner.network.ScannerResponse;
import com.corpit.scanner.util.NetworkUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Response;

import static com.corpit.scanner.ScanActivity.BAR_CODE_RESULT_OK;
import static com.corpit.scanner.ScanActivity.KEY_SCAN_DATA;
import static com.corpit.scanner.ScanActivity.QR_CODE_RESULT_OK;
import static java.lang.System.currentTimeMillis;

public class MainActivity extends BaseActivity {

    public static final String NAME_SETTINGS = "settings";
    public static final String KEY_VENUE = "venue";
    public static final String KEY_SESSION = "session";
    public static final String KEY_SCAN_TYPE = "scanType";
    public static final String KEY_CHECK_TYPE = "checkType";
    public static final String KEY_IS_FORCE_SCAN = "isForceScan";
    public static final String KEY_IS_LOCK = "isLock";

    public static final int FLAG_DOWNLOAD_SUCCESS = 25;
    public static final int FLAG_DOWNLOAD_FAIL = 9;

    private static final int FLAG_NOT_PERMIT = 5;
    private static final int FLAG_PERMITTED = 10;
    private static final int FLAG_FORCE = 15;
    private static final int FLAG_FAILED = 20;

    private static final int SCAN_REQUEST_CODE = 1;

    private RadioGroup checkGroup;

    private Spinner venueSpinner;

    private Spinner sessionSpinner;

    private ArrayAdapter<Session> sessionAdapter;

    private TextView checkText;

    private CheckBox barcodeCheck;

    private CheckBox qrCodeCheck;

    private SwitchCompat forceSwitch;

    private SwitchCompat lockSwitch;

    private ProgressDialog progressDialog;

    private Handler progressHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            progressDialog.dismiss();
            switch (msg.what) {
                case FLAG_DOWNLOAD_SUCCESS:
                    Toast.makeText(MainActivity.this, R.string.msg_download_success, Toast.LENGTH_SHORT).show();
                    initVenueSpinner();
                    initSessionSpinner();
                    break;
                case FLAG_DOWNLOAD_FAIL:
                    Toast.makeText(MainActivity.this, R.string.msg_download_fail, Toast.LENGTH_SHORT).show();
                    break;
                case FLAG_NOT_PERMIT:
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle(R.string.title_not_allowed)
                            .setMessage(R.string.err_not_allowed)
                            .setPositiveButton(R.string.action_ok, null);
                    builder.create().show();
                    break;
                case FLAG_PERMITTED:
                case FLAG_FORCE:
                    final String visitorId = (String) msg.obj;
                    final String sessionId = ((Session) sessionSpinner.getSelectedItem()).getId();
                    saveTransaction(visitorId, sessionId);
                    break;
                case FLAG_FAILED:
                    Toast.makeText(MainActivity.this, R.string.msg_check_fail, Toast.LENGTH_SHORT).show();
                    break;
            }

            super.handleMessage(msg);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        progressDialog = new ProgressDialog(this);

        initVenueSpinner();

        initSessionSpinner();

        initCheckGroup();

        initSwitches();

        initButtons();

        restoreState();
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (requestCode == SCAN_REQUEST_CODE && resultCode != RESULT_CANCELED) {

            String visitorId = "";

            if (resultCode == BAR_CODE_RESULT_OK) {
                visitorId = data.getStringExtra(KEY_SCAN_DATA);
            } else if (resultCode == QR_CODE_RESULT_OK) {
                Visitor visitor = data.getParcelableExtra(KEY_SCAN_DATA);
                visitorId = visitor.getRegisterId();
            }

            if (!NetworkUtil.isNetworkWorking(this)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(R.string.title_error)
                        .setMessage(R.string.err_no_internet)
                        .setPositiveButton(R.string.action_try_again, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                onActivityResult(requestCode, resultCode, data);
                            }
                        });
                builder.create().show();
                return;
            } else {
                processScanResult(visitorId);
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        cacheState();
        super.onDestroy();
    }

    private void initVenueSpinner() {
        venueSpinner = findViewById(R.id.venue_spinner);
        TooltipCompat.setTooltipText(venueSpinner, getString(R.string.desc_venue));

        Cursor cursor = daoSession.getDatabase()
                .rawQuery("SELECT DISTINCT " + SessionDao.Properties.Venue.columnName +
                        " FROM " + SessionDao.TABLENAME, null);

        ArrayList<String> venues = new ArrayList<>();
        while (cursor.moveToNext()) {
            venues.add(cursor.getString(0));
        }
        cursor.close();
        Collections.sort(venues);

        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                R.layout.row_session, venues);
        adapter.addAll();
        venueSpinner.setAdapter(adapter);
        venueSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                List<Session> list = daoSession.getSessionDao().queryBuilder()
                        .where(SessionDao.Properties.Venue.eq(adapter.getItem(position)))
                        .list();
                Collections.sort(list);
                sessionAdapter.clear();
                sessionAdapter.addAll(list);
                sessionSpinner.setSelection(0);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initSessionSpinner() {
        sessionSpinner = findViewById(R.id.session_spinner);
        TooltipCompat.setTooltipText(sessionSpinner, getString(R.string.desc_session));

        sessionAdapter = new ArrayAdapter<>(this, R.layout.row_session);
        sessionSpinner.setAdapter(sessionAdapter);
        sessionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                view.setSelected(true);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initCheckGroup() {
        checkText = findViewById(R.id.check_text);
        checkGroup = findViewById(R.id.check_group);
        barcodeCheck = findViewById(R.id.barcode_check);
        qrCodeCheck = findViewById(R.id.qr_code_check);

        checkGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.checkin_button:
                        checkText.setText(R.string.txt_check_in);
                        break;
                    case R.id.checkout_button:
                        checkText.setText(R.string.txt_check_out);
                        break;
                }
            }
        });

        checkGroup.check(R.id.checkin_button);
    }

    private void initSwitches() {
        forceSwitch = findViewById(R.id.force_switch);

        lockSwitch = findViewById(R.id.lock_switch);
        lockSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Group group = findViewById(R.id.lockable_group);

                for (int viewId : group.getReferencedIds()) {
                    findViewById(viewId).setEnabled(!isChecked);
                }
            }
        });
    }

    private void initButtons() {
        View scanButton = findViewById(R.id.scan_button);
        View dataButton = findViewById(R.id.data_button);
        View refreshButton = findViewById(R.id.refresh_button);

        TooltipCompat.setTooltipText(scanButton, getString(R.string.action_scan));
        TooltipCompat.setTooltipText(dataButton, getString(R.string.action_record_list));
        TooltipCompat.setTooltipText(refreshButton, getString(R.string.action_refresh));

        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkGroup.getCheckedRadioButtonId() != -1) {
                    Intent intent = new Intent(MainActivity.this, ScanActivity.class);

                    int flags = getScanTypes();

                    if (flags == 0) {
                        Toast.makeText(v.getContext(), R.string.msg_no_scan_type, Toast.LENGTH_SHORT).show();
                    } else {
                        intent.putExtra(ScanActivity.KEY_SCAN_TYPE, flags);
                        startActivityForResult(intent, SCAN_REQUEST_CODE);
                    }
                }
            }
        });

        dataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, DataActivity.class);
                startActivity(intent);
            }
        });

        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refresh();
            }
        });
    }

    private void processScanResult(final String visitorId) {
        if (forceSwitch.isChecked()) {
            Message message = progressHandler.obtainMessage(FLAG_FORCE, visitorId);
            progressHandler.sendMessage(message);
            return;
        }

        progressDialog.setMessage(getString(R.string.msg_check_permission));
        progressDialog.show();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                boolean isPermitted = false;
                final String sessionId = ((Session) sessionSpinner.getSelectedItem()).getId();

                try {
                    Response<ScannerResponse<Boolean>> response = ScannerClient.getApiService()
                            .checkPermission(visitorId, sessionId).execute();

                    if (response.isSuccessful()) {
                        //noinspection ConstantConditions
                        isPermitted = response.body().getData();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    progressHandler.sendEmptyMessage(FLAG_FAILED);
                }

                if (isPermitted) {
                    Message message = progressHandler.obtainMessage(FLAG_PERMITTED, visitorId);
                    progressHandler.sendMessage(message);
                } else {
                    progressHandler.sendEmptyMessage(FLAG_NOT_PERMIT);
                }

            }
        };

        new Thread(runnable).start();
    }

    private void saveTransaction(String visitorId, String sessionId) {
        final String msg;
        final String type;

        switch (checkGroup.getCheckedRadioButtonId()) {
            case R.id.checkin_button:
                msg = getString(R.string.msg_checkin);
                type = TransactionType.CHECK_IN;
                break;
            case R.id.checkout_button:
                msg = getString(R.string.msg_checkout);
                type = TransactionType.CHECK_OUT;
                break;
            default:
                return;
        }

        String registrationType = forceSwitch.isChecked() ? RegistrationType.ON_SITE : RegistrationType.PRE_REGISTRATION;

        Transaction transaction = new Transaction.Builder()
                .sessionId(sessionId)
                .visitorId(visitorId)
                .timestamp(currentTimeMillis())
                .deviceName(BluetoothAdapter.getDefaultAdapter().getName())
                .type(type)
                .registrationType(registrationType)
                .sent(false)
                .build();
        daoSession.getTransactionDao().insert(transaction);

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(R.string.title_success)
                .setMessage(msg)
                .setPositiveButton(R.string.action_ok, null);
        builder.create().show();
    }

    @ScanType
    private int getScanTypes() {
        int flags = ScanType.NONE;

        if (barcodeCheck.isChecked()) {
            flags = flags | ScanType.BAR_CODE;
        }

        if (qrCodeCheck.isChecked()) {
            flags = flags | ScanType.QR_CODE;
        }

        return flags;
    }

    private void refresh() {
        progressDialog.setMessage(getString(R.string.msg_download));
        progressDialog.show();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Response<ScannerResponse<List<Session>>> response = ScannerClient.getApiService()
                            .getConferenceList().execute();
                    //noinspection ConstantConditions
                    List<Session> data = response.body().getData();
                    daoSession.getSessionDao().deleteAll();
                    daoSession.getSessionDao().insertInTx(data);
                    progressHandler.sendEmptyMessage(FLAG_DOWNLOAD_SUCCESS);
                } catch (IOException e) {
                    e.printStackTrace();
                    progressHandler.sendEmptyMessage(FLAG_DOWNLOAD_FAIL);
                }
            }
        }).start();
    }

    private void cacheState() {
        SharedPreferences preferences = getSharedPreferences(NAME_SETTINGS, MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(KEY_VENUE, venueSpinner.getSelectedItemPosition());
        editor.putInt(KEY_SESSION, sessionSpinner.getSelectedItemPosition());
        editor.putInt(KEY_SCAN_TYPE, getScanTypes());
        editor.putInt(KEY_CHECK_TYPE, checkGroup.getCheckedRadioButtonId());
        editor.putBoolean(KEY_IS_FORCE_SCAN, forceSwitch.isChecked());
        editor.putBoolean(KEY_IS_LOCK, lockSwitch.isChecked());
        editor.apply();
    }

    private void restoreState() {
        SharedPreferences preferences = getSharedPreferences(NAME_SETTINGS, MODE_PRIVATE);

        venueSpinner.setSelection(preferences.getInt(KEY_VENUE, 0));
        sessionSpinner.setSelection(preferences.getInt(KEY_SESSION, 0));
        @ScanType int flags = preferences.getInt(KEY_SCAN_TYPE, ScanType.BAR_CODE);
        if ((flags | ScanType.BAR_CODE) == flags) {
            barcodeCheck.setChecked(true);
        }
        if ((flags | ScanType.QR_CODE) == flags) {
            qrCodeCheck.setChecked(true);
        }
        checkGroup.check(preferences.getInt(KEY_CHECK_TYPE, R.id.checkin_button));
        forceSwitch.setChecked(preferences.getBoolean(KEY_IS_FORCE_SCAN, false));
        lockSwitch.setChecked(preferences.getBoolean(KEY_IS_LOCK, lockSwitch.isChecked()));
    }

}
