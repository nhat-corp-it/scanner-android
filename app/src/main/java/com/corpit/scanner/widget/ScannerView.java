package com.corpit.scanner.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;

import com.google.zxing.PlanarYUVLuminanceSource;

import me.dm7.barcodescanner.core.IViewFinder;
import me.dm7.barcodescanner.core.ViewFinderView;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ScannerView extends ZXingScannerView {

    public ScannerView(Context context) {
        super(context);
    }

    public ScannerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @Override
    protected IViewFinder createViewFinderView(Context context) {
        return new CustomViewFinder(context);
    }

    @Override
    public PlanarYUVLuminanceSource buildLuminanceSource(byte[] data, int width, int height) {
        return new PlanarYUVLuminanceSource(data, width, height, 0, 0, width, height, false);
    }

    private class CustomViewFinder extends ViewFinderView {

        public CustomViewFinder(Context context) {
            super(context);
        }

        public CustomViewFinder(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        @Override
        public void onDraw(Canvas canvas) {
            drawLaser(canvas);
            drawViewFinderBorder(canvas);
        }
    }
}
