package com.corpit.scanner;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import com.corpit.scanner.model.DaoSession;

public abstract class BaseActivity extends AppCompatActivity {

    protected DaoSession daoSession;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        daoSession = ((ScannerApp) getApplication()).getDaoSession();
    }

}
