package com.corpit.scanner;

import android.app.Application;

import com.corpit.scanner.model.DaoMaster;
import com.corpit.scanner.model.DaoSession;

import org.greenrobot.greendao.database.Database;

public class ScannerApp extends Application {

    private DaoSession daoSession;

    private static final String DATABASE_NAME = "Scanner2018v1.db";

    @Override
    public void onCreate() {
        super.onCreate();

        DaoMaster.OpenHelper helper = new DaoMaster.DevOpenHelper(this, DATABASE_NAME);
        Database db = helper.getWritableDb();
        daoSession = new DaoMaster(db).newSession();
    }

    public DaoSession getDaoSession() {
        return daoSession;
    }

}
