package com.corpit.scanner;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.corpit.scanner.model.Transaction;
import com.corpit.scanner.model.Transaction.TransactionType;
import com.corpit.scanner.model.TransactionDao;
import com.corpit.scanner.network.QueryMaps;
import com.corpit.scanner.network.ScannerClient;
import com.corpit.scanner.network.ScannerResponse;
import com.corpit.scanner.util.DateConverter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

public class DataActivity extends BaseActivity {

    public static final int FLAG_SYNC_SUCCESS = 25;

    public static final int FLAG_SYNC_FAIL = 9;

    private DataAdapter dataAdapter;

    private ProgressDialog progressDialog;

    private Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == FLAG_SYNC_SUCCESS) {
                progressDialog.dismiss();
                dataAdapter.notifyDataSetChanged();
                Toast.makeText(DataActivity.this, R.string.msg_synced, Toast.LENGTH_SHORT).show();
            } else if (msg.what == FLAG_SYNC_FAIL) {
                progressDialog.dismiss();
                Toast.makeText(DataActivity.this, R.string.msg_sync_failed, Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.msg_syncing));

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        RecyclerView listView = findViewById(R.id.list);

        List<Transaction> transactions = daoSession.getTransactionDao().loadAll();
        dataAdapter = new DataAdapter(transactions);
        listView.setAdapter(dataAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_data, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_sync) {
            sync();
        }
        return super.onOptionsItemSelected(item);
    }

    private void sync() {
        progressDialog.show();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                List<Transaction> transactions = daoSession.getTransactionDao()
                        .queryBuilder()
                        .where(TransactionDao.Properties.Sent.eq(false))
                        .list();
                try {
                    for (Transaction transaction : transactions) {

                        Response<ScannerResponse<Boolean>> response = ScannerClient.getApiService()
                                .uploadRecord(QueryMaps.uploadRecord(transaction)).execute();

                        //noinspection ConstantConditions
                        boolean isSent = response.body().getData();

                        transaction.setSent(isSent);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                daoSession.getTransactionDao().updateInTx(transactions);
                mHandler.sendEmptyMessage(FLAG_SYNC_SUCCESS);
            }
        };

        new Thread(runnable).start();
    }

    private class DataAdapter extends RecyclerView.Adapter<DataAdapter.DataHolder> {

        private List<Transaction> mData;

        DataAdapter(List<Transaction> data) {
            mData = data;
        }

        @NonNull
        @Override
        public DataHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_data, parent, false);
            return new DataHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull DataHolder holder, int position) {
            Transaction transaction = mData.get(position);

            holder.visitorId.setText(transaction.getVisitorId());
            holder.timestamp.setText(DateConverter.toText(transaction.getTimestamp(), DateConverter.CLIENT_DATE_FORMAT));
            holder.type.setText(transaction.getType());
            switch (transaction.getType()) {
                case TransactionType.CHECK_IN:
                    holder.type.setTextColor(getResources().getColor(R.color.green));
                    break;
                case TransactionType.CHECK_OUT:
                    holder.type.setTextColor(getResources().getColor(R.color.red));
                    break;
            }
            holder.regType.setText(transaction.getRegistrationType());

            if (transaction.getSent()) {
                holder.syncIndicator.setBackgroundColor(getResources().getColor(R.color.blue));
            }

        }

        public void clear() {
            mData = new ArrayList<>();
            notifyDataSetChanged();
        }

        @Override
        public int getItemCount() {
            return mData.size();
        }

        class DataHolder extends RecyclerView.ViewHolder {

            View syncIndicator;
            TextView visitorId;
            TextView timestamp;
            TextView type;
            TextView regType;
            View root;

            DataHolder(View view) {
                super(view);
                root = view;
                syncIndicator = view.findViewById(R.id.sync_indicator);
                visitorId = view.findViewById(R.id.visitor_id);
                timestamp = view.findViewById(R.id.timestamp);
                type = view.findViewById(R.id.type);
                regType = view.findViewById(R.id.reg_type);
            }
        }
    }

}
