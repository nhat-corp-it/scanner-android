package com.corpit.scanner.model;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.ToOne;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.DaoException;

@Entity
public class SessionVisitor {

    @Id(autoincrement = true)
    private Long id;

    @Index
    private String sessionId;

    @Index
    private String visitorId;

    @ToOne(joinProperty = "visitorId")
    private VisitorEntity visitor;

    @ToOne(joinProperty = "sessionId")
    private Session session;

    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    @Generated(hash = 101933318)
    private transient SessionVisitorDao myDao;

    @Generated(hash = 753740249)
    public SessionVisitor(Long id, String sessionId, String visitorId) {
        this.id = id;
        this.sessionId = sessionId;
        this.visitorId = visitorId;
    }

    @Generated(hash = 1247750794)
    public SessionVisitor() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSessionId() {
        return this.sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getVisitorId() {
        return this.visitorId;
    }

    public void setVisitorId(String visitorId) {
        this.visitorId = visitorId;
    }

    @Generated(hash = 62500079)
    private transient String visitor__resolvedKey;

    /** To-one relationship, resolved on first access. */
    @Generated(hash = 457510735)
    public VisitorEntity getVisitor() {
        String __key = this.visitorId;
        if (visitor__resolvedKey == null || visitor__resolvedKey != __key) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            VisitorEntityDao targetDao = daoSession.getVisitorEntityDao();
            VisitorEntity visitorNew = targetDao.load(__key);
            synchronized (this) {
                visitor = visitorNew;
                visitor__resolvedKey = __key;
            }
        }
        return visitor;
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 208197180)
    public void setVisitor(VisitorEntity visitor) {
        synchronized (this) {
            this.visitor = visitor;
            visitorId = visitor == null ? null : visitor.getId();
            visitor__resolvedKey = visitorId;
        }
    }

    @Generated(hash = 193533454)
    private transient String session__resolvedKey;

    /** To-one relationship, resolved on first access. */
    @Generated(hash = 442496962)
    public Session getSession() {
        String __key = this.sessionId;
        if (session__resolvedKey == null || session__resolvedKey != __key) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            SessionDao targetDao = daoSession.getSessionDao();
            Session sessionNew = targetDao.load(__key);
            synchronized (this) {
                session = sessionNew;
                session__resolvedKey = __key;
            }
        }
        return session;
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 1772738027)
    public void setSession(Session session) {
        synchronized (this) {
            this.session = session;
            sessionId = session == null ? null : session.getId();
            session__resolvedKey = sessionId;
        }
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 1025459582)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getSessionVisitorDao() : null;
    }
}
