package com.corpit.scanner.model;

import android.support.annotation.NonNull;

import com.squareup.moshi.Json;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

@Entity
public class Session implements Comparable<Session> {

    @Id
    @Json(name = "SID")
    private String id;

    @Json(name = "Name")
    private String name;

    @Json(name = "Time")
    private String time;

    @Json(name = "Venue")
    private String venue;

    @Json(name = "Speaker")
    private String speaker;

    @Generated(hash = 192411639)
    public Session(String id, String name, String time, String venue, String speaker) {
        this.id = id;
        this.name = name;
        this.time = time;
        this.venue = venue;
        this.speaker = speaker;
    }

    @Generated(hash = 1317889643)
    public Session() {
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int compareTo(@NonNull Session o) {
        return name.compareTo(o.name);
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTime() {
        return this.time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getVenue() {
        return this.venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getSpeaker() {
        return this.speaker;
    }

    public void setSpeaker(String speaker) {
        this.speaker = speaker;
    }
}
