package com.corpit.scanner.model;

import android.support.annotation.StringDef;

import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.ToOne;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Entity
public class Transaction {

    private Transaction(Builder builder) {
        setSessionId(builder.sessionId);
        setVisitorId(builder.visitorId);
        setTimestamp(builder.timestamp);
        setDeviceName(builder.deviceName);
        setType(builder.type);
        setRegistrationType(builder.registrationType);
        setSent(builder.sent);
    }

    @StringDef({TransactionType.CHECK_IN, TransactionType.CHECK_OUT})
    @Retention(RetentionPolicy.SOURCE)
    public @interface TransactionType {
        String CHECK_IN = "Checkin";
        String CHECK_OUT = "Checkout";
    }

    @StringDef({RegistrationType.PRE_REGISTRATION, RegistrationType.ON_SITE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface RegistrationType {
        String PRE_REGISTRATION = "PreReg";
        String ON_SITE = "OnSite";
    }

    @Id(autoincrement = true)
    private Long id;

    private String sessionId;

    private String visitorId;

    private long timestamp;

    private String deviceName;

    @TransactionType
    private String type;

    @RegistrationType
    private String registrationType;

    private boolean sent;

    @ToOne(joinProperty = "visitorId")
    private VisitorEntity visitor;

    @ToOne(joinProperty = "sessionId")
    private Session session;

    /**
     * Used to resolve relations
     */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /**
     * Used for active entity operations.
     */
    @Generated(hash = 947191939)
    private transient TransactionDao myDao;

    @Generated(hash = 244716542)
    public Transaction(Long id, String sessionId, String visitorId, long timestamp,
                       String deviceName, String type, String registrationType, boolean sent) {
        this.id = id;
        this.sessionId = sessionId;
        this.visitorId = visitorId;
        this.timestamp = timestamp;
        this.deviceName = deviceName;
        this.type = type;
        this.registrationType = registrationType;
        this.sent = sent;
    }

    @Generated(hash = 750986268)
    public Transaction() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSessionId() {
        return this.sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getVisitorId() {
        return this.visitorId;
    }

    public void setVisitorId(String visitorId) {
        this.visitorId = visitorId;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getDeviceName() {
        return this.deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRegistrationType() {
        return this.registrationType;
    }

    public void setRegistrationType(String registrationType) {
        this.registrationType = registrationType;
    }

    public boolean getSent() {
        return this.sent;
    }

    public void setSent(boolean sent) {
        this.sent = sent;
    }

    @Generated(hash = 62500079)
    private transient String visitor__resolvedKey;

    /**
     * To-one relationship, resolved on first access.
     */
    @Generated(hash = 457510735)
    public VisitorEntity getVisitor() {
        String __key = this.visitorId;
        if (visitor__resolvedKey == null || visitor__resolvedKey != __key) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            VisitorEntityDao targetDao = daoSession.getVisitorEntityDao();
            VisitorEntity visitorNew = targetDao.load(__key);
            synchronized (this) {
                visitor = visitorNew;
                visitor__resolvedKey = __key;
            }
        }
        return visitor;
    }

    /**
     * called by internal mechanisms, do not call yourself.
     */
    @Generated(hash = 208197180)
    public void setVisitor(VisitorEntity visitor) {
        synchronized (this) {
            this.visitor = visitor;
            visitorId = visitor == null ? null : visitor.getId();
            visitor__resolvedKey = visitorId;
        }
    }

    @Generated(hash = 193533454)
    private transient String session__resolvedKey;

    /**
     * To-one relationship, resolved on first access.
     */
    @Generated(hash = 442496962)
    public Session getSession() {
        String __key = this.sessionId;
        if (session__resolvedKey == null || session__resolvedKey != __key) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            SessionDao targetDao = daoSession.getSessionDao();
            Session sessionNew = targetDao.load(__key);
            synchronized (this) {
                session = sessionNew;
                session__resolvedKey = __key;
            }
        }
        return session;
    }

    /**
     * called by internal mechanisms, do not call yourself.
     */
    @Generated(hash = 1772738027)
    public void setSession(Session session) {
        synchronized (this) {
            this.session = session;
            sessionId = session == null ? null : session.getId();
            session__resolvedKey = sessionId;
        }
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 511087935)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getTransactionDao() : null;
    }

    public static final class Builder {
        private String sessionId;
        private String visitorId;
        private long timestamp;
        private String deviceName;
        private String type;
        private String registrationType;
        private boolean sent;

        public Builder() {
        }

        public Builder sessionId(String val) {
            sessionId = val;
            return this;
        }

        public Builder visitorId(String val) {
            visitorId = val;
            return this;
        }

        public Builder timestamp(long val) {
            timestamp = val;
            return this;
        }

        public Builder deviceName(String val) {
            deviceName = val;
            return this;
        }

        public Builder type(String val) {
            type = val;
            return this;
        }

        public Builder registrationType(String val) {
            registrationType = val;
            return this;
        }

        public Builder sent(boolean val) {
            sent = val;
            return this;
        }

        public Transaction build() {
            return new Transaction(this);
        }
    }
}
