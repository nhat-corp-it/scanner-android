package com.corpit.scanner.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Visitor implements Parcelable {

    private long id;

    private String name;

    private String email;

    private String company;

    private String country;

    private String jobTitle;

    private String phone;

    private String registerId;

    private String qrCode;

    private String permissions;

    private long checkInTime;

    private long checkOutTime;

    public Visitor(long id, String name, String email, String company, String country, String jobTitle, String phone, String registerId, String qrCode, String permissions, long checkInTime, long checkOutTime) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.company = company;
        this.country = country;
        this.jobTitle = jobTitle;
        this.phone = phone;
        this.registerId = registerId;
        this.qrCode = qrCode;
        this.permissions = permissions;
        this.checkInTime = checkInTime;
        this.checkOutTime = checkOutTime;
    }

    public Visitor() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRegisterId() {
        return registerId;
    }

    public void setRegisterId(String registerId) {
        this.registerId = registerId;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public long getCheckInTime() {
        return checkInTime;
    }

    public void setCheckInTime(long checkInTime) {
        this.checkInTime = checkInTime;
    }

    public long getCheckOutTime() {
        return checkOutTime;
    }

    public void setCheckOutTime(long checkOutTime) {
        this.checkOutTime = checkOutTime;
    }

    public String getPermissions() {
        return permissions;
    }

    public void setPermissions(String permissions) {
        this.permissions = permissions;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.name);
        dest.writeString(this.email);
        dest.writeString(this.company);
        dest.writeString(this.country);
        dest.writeString(this.jobTitle);
        dest.writeString(this.phone);
        dest.writeString(this.registerId);
        dest.writeString(this.qrCode);
        dest.writeString(this.permissions);
        dest.writeLong(this.checkInTime);
        dest.writeLong(this.checkOutTime);
    }

    protected Visitor(Parcel in) {
        this.id = in.readLong();
        this.name = in.readString();
        this.email = in.readString();
        this.company = in.readString();
        this.country = in.readString();
        this.jobTitle = in.readString();
        this.phone = in.readString();
        this.registerId = in.readString();
        this.qrCode = in.readString();
        this.permissions = in.readString();
        this.checkInTime = in.readLong();
        this.checkOutTime = in.readLong();
    }

    public static final Creator<Visitor> CREATOR = new Creator<Visitor>() {
        @Override
        public Visitor createFromParcel(Parcel source) {
            return new Visitor(source);
        }

        @Override
        public Visitor[] newArray(int size) {
            return new Visitor[size];
        }
    };

    public VisitorEntity toEntity(){
        VisitorEntity entity = new VisitorEntity();
        entity.setId(registerId);
        entity.setFirstName(name);
        entity.setLastName(name);
        entity.setCompany(company);
        return entity;
    }
}
