package com.corpit.scanner;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;

import com.corpit.scanner.model.Visitor;
import com.corpit.scanner.util.QrDecodeHelper;
import com.corpit.scanner.util.QrDecodeHelper.QrCodeType;
import com.corpit.scanner.widget.ScannerView;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

import static com.corpit.scanner.util.QrDecodeHelper.toVisitor;

public class ScanActivity extends BaseActivity implements ZXingScannerView.ResultHandler {

    private static final int REQUEST_CAMERA_REQUEST_CODE = 101;

    public static final int QR_CODE_RESULT_OK = 10;

    public static final int BAR_CODE_RESULT_OK = 11;

    public static final long VIBRATE_TIME = 200L;

    public static final String KEY_SCAN_DATA = "scan_data";

    public static final String KEY_SCAN_TYPE = "scan_type";

    @ScanType
    private int flags;

    @IntDef(flag = true,
            value = {ScanType.NONE, ScanType.QR_CODE, ScanType.BAR_CODE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface ScanType {
        int NONE = 0;
        int QR_CODE = 1;
        int BAR_CODE = 1 << 1;
    }

    private ZXingScannerView mScannerView;

    private Vibrator vibrator;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mScannerView = new ScannerView(this);

        setContentView(mScannerView);

        flags = getIntent().getIntExtra(KEY_SCAN_TYPE, 0);

        if (checkPermission()) {
            List<BarcodeFormat> barcodeFormats = new ArrayList<>();

            if ((flags | ScanType.BAR_CODE) == flags) {
                barcodeFormats.add(BarcodeFormat.CODE_39);
            }
            if ((flags | ScanType.QR_CODE) == flags) {
                barcodeFormats.add(BarcodeFormat.QR_CODE);
            }

            mScannerView.setFormats(barcodeFormats);

            mScannerView.setShouldScaleToFill(true);

            mScannerView.setAutoFocus(true);

            mScannerView.setAspectTolerance(0.5f);
        }

        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (outState != null) {
            outState.putInt(KEY_SCAN_TYPE, flags);
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            flags = savedInstanceState.getInt(KEY_SCAN_TYPE);
        }
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA_REQUEST_CODE:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setResult(RESULT_CANCELED);
                    finish();
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle(R.string.title_camera_permission)
                            .setMessage(R.string.error_camera_permission)
                            .setPositiveButton(R.string.action_ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    setResult(RESULT_CANCELED);
                                    finish();
                                }
                            })
                            .setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    setResult(RESULT_CANCELED);
                                    finish();
                                }
                            });
                    builder.create().show();
                }
                break;
        }
    }

    private boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},
                    REQUEST_CAMERA_REQUEST_CODE);
            return false;
        }

        return true;
    }

    @Override
    public void handleResult(Result rawResult) {

        vibrator.vibrate(VIBRATE_TIME);

        switch (rawResult.getBarcodeFormat()) {
            case CODE_39:
                handleBarcode(rawResult.getText());
                break;
            case QR_CODE:
                handleQrCode(rawResult.getText());
                break;
        }
    }

    private void handleQrCode(String text) {
        String decodedString = QrDecodeHelper.decrypt(text);
        if (BuildConfig.DEBUG) {
            decodedString = "vINNA SUN ^inna.sun@ul.com^UL ENVIRONMENT^SINGAPORE^ASEAN BDM^65 9640 7659^767^1111000011";
        }

        if (QrDecodeHelper.isValidQrCode(decodedString, QrCodeType.VISITOR)) {
            Visitor visitor = toVisitor(decodedString);
            Intent intent = new Intent();
            intent.putExtra(KEY_SCAN_DATA, visitor);
            setResult(QR_CODE_RESULT_OK, intent);
            finish();
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.title_error)
                    .setMessage(R.string.err_invalid_qr)
                    .setPositiveButton(R.string.action_try_again, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mScannerView.resumeCameraPreview(ScanActivity.this);
                        }
                    })
                    .setNegativeButton(R.string.action_cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            setResult(RESULT_CANCELED);
                            finish();
                        }
                    });
            builder.create().show();
        }
    }

    private void handleBarcode(String text) {
        if (BuildConfig.DEBUG) {
            text = "Z7N4WP4PX6V";
//            text = "D2N7QR3SZ29";
        }

        if (text.length() == 11) {
            Intent intent = new Intent();
            intent.putExtra(KEY_SCAN_DATA, text);
            setResult(BAR_CODE_RESULT_OK, intent);
            finish();
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.title_error)
                    .setMessage(R.string.err_invalid_barcode)
                    .setPositiveButton(R.string.action_try_again, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mScannerView.resumeCameraPreview(ScanActivity.this);
                        }
                    })
                    .setNegativeButton(R.string.action_cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            setResult(RESULT_CANCELED);
                            finish();
                        }
                    });
            builder.create().show();
        }
    }

}
